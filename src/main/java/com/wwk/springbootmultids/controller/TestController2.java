package com.wwk.springbootmultids.controller;

import com.wwk.springbootmultids.entity.User;
import com.wwk.springbootmultids.service.TestService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@EnableAutoConfiguration
@RestController
public class TestController2 {

    @Resource
    private TestService testService;

    @RequestMapping("/test1")
    public String test(){
     for(User d:testService.getList()){
         System.out.println(d);
     }
        for(User d:testService.getListByDs1()){
            System.out.println(d);
        }
        return"ok";
    }
}
