package com.wwk.springbootmultids;

import com.wwk.springbootmultids.dynamic.DynamicDataSourceRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({DynamicDataSourceRegister.class})
public class SpringBootMultidsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMultidsApplication.class, args);
    }

}
