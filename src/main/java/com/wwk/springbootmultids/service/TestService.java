package com.wwk.springbootmultids.service;

import com.wwk.springbootmultids.dao.TestDao;
import com.wwk.springbootmultids.dynamic.TargetDataSource;
import com.wwk.springbootmultids.entity.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestService {

    @Resource
    private TestDao testDao;

    /**
     * 不指定数据源使用默认数据源
     * @return
     */

    public List<User> getList(){
        return testDao.getList();
    }

    /**
     * 指定数据源
     * @return
     */
    @TargetDataSource("ds1")
    public List<User> getListByDs1(){
        return testDao.getListByDs1();
    }
}
